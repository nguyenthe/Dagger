package com.example.dagger.component;

import com.example.dagger.MainActivity;
import com.example.dagger.module.ProfileModule;
import com.example.dagger.scope.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(dependencies = NetComponent.class, modules = ProfileModule.class)
public interface ProfileComponent {
    void inject(MainActivity activity);
}
