package com.example.dagger.component;

import com.example.dagger.MainActivity;
import com.example.dagger.module.AppModule;
import com.example.dagger.module.NetModule;
import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {NetModule.class, AppModule.class})
public interface NetComponent {
    Retrofit retrofit();
    Gson gson();
    OkHttpClient okHttpClient();
}
