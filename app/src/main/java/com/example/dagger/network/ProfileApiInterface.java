package com.example.dagger.network;

import com.example.dagger.domain.Repository;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ProfileApiInterface {
    @GET("users/{user}/repos")
    Call<ArrayList<Repository>> getProfileUser(@Path("user") String userName);
}
