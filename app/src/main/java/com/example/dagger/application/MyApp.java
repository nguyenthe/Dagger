package com.example.dagger.application;

import android.app.Application;

import com.example.dagger.component.DaggerNetComponent;
import com.example.dagger.component.DaggerProfileComponent;
import com.example.dagger.component.NetComponent;
import com.example.dagger.component.ProfileComponent;
import com.example.dagger.module.AppModule;
import com.example.dagger.module.NetModule;
import com.example.dagger.module.ProfileModule;

public class MyApp extends Application {

    private NetComponent mNetComponent;
    private ProfileComponent profileComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mNetComponent = DaggerNetComponent.builder()
                        .appModule(new AppModule(this))
                        .netModule(new NetModule("https://api.github.com"))
                        .build();

        profileComponent = DaggerProfileComponent.builder()
                            .netComponent(mNetComponent)
                            .profileModule(new ProfileModule())
                            .build();
    }

    public NetComponent getNetComponent() {
        return mNetComponent;
    }

    public ProfileComponent getProfileComponent() {
        return profileComponent;
    }
}
