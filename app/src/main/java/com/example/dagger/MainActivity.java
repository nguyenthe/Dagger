package com.example.dagger;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.dagger.application.MyApp;
import com.example.dagger.domain.Repository;
import com.example.dagger.network.ProfileApiInterface;
import com.google.gson.Gson;

import java.util.ArrayList;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    @Inject Retrofit retrofit;
    @Inject Gson gson;
    @Inject OkHttpClient okHttpClient;

    @Inject
    ProfileApiInterface profileApiInterface;

    private TextView tvName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        ((MyApp) getApplication()).getProfileComponent().inject(this);
        getProfile();
    }

    private void getProfile() {
        Call<ArrayList<Repository>> call = profileApiInterface.getProfileUser("codepath");
        call.enqueue(new Callback<ArrayList<Repository>>() {
            @Override
            public void onResponse(Call<ArrayList<Repository>> call, Response<ArrayList<Repository>> response) {
                if(response.body() == null || response.body().size() == 0 || response.body().get(0) == null)
                    return;
                Repository repository = response.body().get(0);
                tvName.setText(repository.getFullName());
            }

            @Override
            public void onFailure(Call<ArrayList<Repository>> call, Throwable t) {
            }
        });
    }

    private void initView() {
        tvName = findViewById(R.id.tv_name);
    }
}