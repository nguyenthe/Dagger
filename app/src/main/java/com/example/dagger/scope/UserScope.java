package com.example.dagger.scope;

import javax.inject.Scope;

@Scope
public @interface UserScope {
}