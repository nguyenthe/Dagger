package com.example.dagger.module;

import com.example.dagger.network.ProfileApiInterface;
import com.example.dagger.scope.ActivityScope;
import com.example.dagger.scope.UserScope;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ProfileModule {

    @Provides
    @ActivityScope
    public ProfileApiInterface providesProfileInterface(Retrofit retrofit) {
        return retrofit.create(ProfileApiInterface.class);
    }
}
