package com.example.dagger.domain;

import java.io.Serializable;

public class Repository implements Serializable {

    String name;
    String fullName;
    String description;

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public String getDescription() {
        return description;
    }
}
