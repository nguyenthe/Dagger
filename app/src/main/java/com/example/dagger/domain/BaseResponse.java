package com.example.dagger.domain;

import java.io.Serializable;

public class BaseResponse<T> implements Serializable {
    private T data;
    private okhttp3.Response rawResponse;

    public T getData() {
        return data;
    }
    public void setData(T data) {
        this.data = data;
    }
    public okhttp3.Response raw() {
        return rawResponse;
    }
}

